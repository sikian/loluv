# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

summ_click = ->
	$(".summ").click ->
		summ = $(this)
		cont = summ.parent('.summ_container')
		name = summ.next().children('input[id*="username"]').val()
		selected = cont.hasClass("selected")
		
		$('.selected > .summ_desc').slideUp ->
			summ = $(this).prev()
			name = summ.next().children('input[id*="name"]').val()
			summ.parent('.summ_container').removeClass 'selected'
			
			if(name != "" && summ.text() != name)
				summ.fadeOut ->
					summ.text(name)
					summ.fadeIn()
			
		cont.addClass 'selected'			
		cont.children('.summ_desc').slideDown()	


$ ->
	$('.selected').children('.summ_desc').show();
	summ_click()
	
	$('.summ').each ->
		username = $(this).next().children('input[id*="username"]').val()
		
		if username.length > 0
			$(this).text username
		else
			$(this).text "Invocador"

	$('.more_summoners').click ->
		$(this).parent('#right').append(summ_text).append(this)
		
