# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

click_up = ->
	$('#user_positions').children('div').find('.up').click ->
		position = $(this).parents('.position')
		position.insertBefore(position.prev())
		
		restore_arrows()

click_down = ->	
	$('#user_positions').children('div').find('.down').click ->
		position = $(this).parents('.position')
		position.insertAfter(position.next())
		
		restore_arrows()
		
restore_arrows = ->
	positions = $('#user_positions').children('.position')
	
	if positions.length > 1
		positions.not(':first').not(':last').children('.arrows').html("<a class='up'>^</a><a class='down'>v</a> ")
		positions.first().children('.arrows').html("<a class='down'>v</a> ")
		positions.last().children('.arrows').html("<a class='up'>^</a> ")
	else
		positions.children('.arrows').html("<a>·</a>")
	
	click_up()
	click_down()
	click_remove()
	
click_add = ->
	set_missing_roles_height
	
	$('.add_role').click ->
		add_butt = $(this)
		miss = $('#missing_roles')
		miss.slideDown()
			
		miss.find('a').click ->
			pos = $(this).attr("id")
			
			# Avoid having repeated positions
			if($('.position#'+pos).text() == "")
				pos_html = '<div class="position" id="'+pos+'"><div class="arrows">^ </div> '+pos.substr(0,1).toUpperCase()+pos.substr(1)+'<a class="remove_role">x</a></div'
				add_butt.before(pos_html)
				update_roles()			
							
				if($('#missing_roles a').length == 1)
					miss.hide()
				
				restore_arrows()
				$(this).remove()
				
				set_missing_roles_height()
		
	$('#missing_roles').mouseleave ->
		$(this).slideUp()
		
	

# Removes a role
click_remove = ->
	$('.remove_role').click ->
		role = $(this).parent()
		role_name = role.attr('id')
		
		# Avoid having repeated positions (not working!)
		if($('#missing_roles #'+role.attr("id")).text() == "")
			$('#missing_roles').prepend('<a id="'+role_name+'">'+role_name+'</a>')
			role.remove()
			
			update_roles()
			set_missing_roles_height()
			restore_arrows()

set_missing_roles_height = ->
	mis_num = $('#missing_roles a').length
	$('#missing_roles').css('top',-mis_num*9+5)
	
# Updates "update_roles" url
update_roles = ->
	roles_array = $('#update_roles a').attr('href').split("/").slice(0,-1)

	roles = ""
	$('#user_positions .position').each ->
		roles += $(this).attr('id').charAt(0)
	
	new_url = roles_array.join("/")+"/"+roles
	$('#update_roles a').attr('href',new_url)
	
$ ->
	# Only use script if there is a #user_position
	if $('#user_positions').length > 0
		positions = $('#user_positions').children('.position')
		
		if positions.length > 1
			positions.not(':first').not(':last').prepend("<div class='arrows'><a class='up'>^</a><a class='down'>v</a></div> ")
			positions.first().prepend("<div class='arrows'><a class='down'>v</a></div> ")
			positions.last().prepend("<div class='arrows'><a class='up'>^</a></div> ")
		else
			positions.first().prepend("<div class='arrows'><a>·</a></div> ")
		
		click_up()
		click_down()
		click_add()
		click_remove()
		set_missing_roles_height()
		update_roles()
