class ApplicationController < ActionController::Base
  protect_from_forgery
  
  helper_method :current_user, :authenticate, :access_denied
	private
  def current_user_session
		return @current_user_session if defined?(@current_user_session)
		@current_user_session = UserSession.find
	end
	def current_user
		return @current_user if defined?(@current_user)
		@current_user = current_user_session && current_user_session.record
	end
	
	def authenticate
		current_user? ? true : access_denied
	end
	def access_denied
		redirect_to :login, :notice => "No est&aacute;s logueado."
	end
end
