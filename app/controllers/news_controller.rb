#!/bin/env ruby
# encoding: utf-8

class NewsController < ApplicationController
  before_filter :check_auth, :only => [:edit, :update, :destroy, :new, :create]
  
  # GET /news
  # GET /news.json
  def index
		now = Time.new.utc.to_s[0..-5]
    @news = News.where("published < Datetime('#{now}')").all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @news }
    end
  end

  # GET /news/1
  # GET /news/1.json
  def show
    @news = News.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @news }
    end
  end

  # GET /news/new
  # GET /news/new.json
  def new
		@news = News.new

		respond_to do |format|
			format.html # new.html.erb
			format.json { render json: @news }
		end
  end

  # GET /news/1/edit
  def edit
    @news = News.find(params[:id])
  end

  # POST /news
  # POST /news.json
  def create
    @news = News.new(params[:news])
    
    @news.user_id = current_user.id

    respond_to do |format|
      if @news.save
        format.html { redirect_to @news, notice: 'News was successfully created.' }
        format.json { render json: @news, status: :created, location: @news }
      else
        format.html { render action: "new" }
        format.json { render json: @news.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /news/1
  # PUT /news/1.json
  def update
    @news = News.find(params[:id])

    respond_to do |format|
      if @news.update_attributes(params[:news])
        format.html { redirect_to @news, notice: 'News was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @news.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /news/1
  # DELETE /news/1.json
  def destroy
    @news = News.find(params[:id])
    @news.destroy

    respond_to do |format|
      format.html { redirect_to news_index_url }
      format.json { head :no_content }
    end
  end
  
  def check_auth
		@access_granted = false
		
		if current_user
			if current_user.role == 1
				@access_granted = true
			end
		end
		
		unless @access_granted
			redirect_to "/news", notice: 'No tienes permisos para tal acción.'
		end
	end
end
