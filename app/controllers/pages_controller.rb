#!/bin/env ruby
# encoding: utf-8

class PagesController < ApplicationController
	def index
		@self_wrapped = true
		#~ render :action => 'users'
	end
	
	def info
	end
	
	def match_making_info
	end
	
	# ADMIN
	def admin_send_email
		if current_user && current_user.role == 1
			#~ @ps = params if params
			@ps ||= params
			content = @ps['content'].to_s.gsub(/\n/, '<br />').html_safe
			subject = @ps['subject']
			
			if content || subject
				if !content
					flash[:alert] = "No hay ningún contenido."
				elsif !subject
					flash[:alert] = "No hay ningún asunto."
				else
					teams = Team.where("verified >= #{VERIFIED_MIN}").all
					flash[:notice] = "Sending emails:<br />"
					
					teams.each do |team|
						team.users.each do |user|
							mail = UserMailer.mass_email(user.uvuser,subject,content).deliver
							if mail
								flash[:notice] << "#{user.username}: Success<br />"
							else
								flash[:alert] << "#{user.username}: Failed<br />"
							end
						end
					end
				end	
			end
		else
			redirect_to :teams
		end
	end
		
	def make_matches
		if current_user && current_user.role == 1
			matches = Matches.all
			
			if matches.size == 0
				require 'digest'
				flash[:notice] = "Seeds are being constructed:<br />"
				flash[:alert] = String.new
				
				# OUTPUT
				@gen_seed = String.new
				@team_seed = Array.new
			
				general_seed = String.new
				
				# GET HASH
				ver_teams = Team.where("verified >= #{VERIFIED_MIN}")
				
				ver_teams.each do |team|
					team.users.each do |user|
						general_seed << user.seed if user.seed
					end
				end
				
				@gen_seed = general_seed
				#~ File.open(Rails.root.join("config","initializers","custom_constants.rb"),"a") do |f|
					#~ f.write "GENERAL_SEED = '#{general_seed}'"
				#~ end
				
				# TEAM HASH
				ver_teams.each do |team|
					seed = general_seed + team.name
					
					team.users.each do |user|
						seed << user.username
					end
					
					@team_seed[team.id] = seed
					hash = Digest::MD5.hexdigest(seed)
					team.matching_hash = hash
					
					if team.save
						flash[:notice] << "Hash for #{team.name} saved.<br />"
					else
						flash[:alert] << "Problem with #{team.name}: #{team.id}.<br />"
					end
				end

				@teams = ver_teams
				@ordered_teams = Team.where("verified >= #{VERIFIED_MIN}").order("matching_hash ASC").all
				
				
				# GENERATE MATCHES (suposing pair)
				m_teams = @ordered_teams.clone
				while m_teams.size > 1
					match = Match.new
					match.team1_id = m_teams.pop.id
					match.team2_id = m_teams.pop.id
					match.save
				end
			else
				redirect_to :matches, :alert => "Los emparejamientos ya están hechos"
			end
		else
			redirect_to :matches
		end
	end	
end
