#!/bin/env ruby
# encoding: utf-8

class TeamsController < ApplicationController
	before_filter :check_auth, :only => [:edit, :update, :destroy, :send_welcome]

	# PREMADE and RECRUIT defs
	REC_STR = 'recruit'
	PRE_STR = 'premade'
	REC_DB = 1
	PRE_DB = 0
	
  # GET /teams
  # GET /teams.json
  def index
		@type = params[:type]
		
		if @type == REC_STR
			@teams = Team.where("team_type IS #{REC_DB} AND verified >= #{VER_REC_MIN}").all
			@not_ver_teams = Team.where("team_type IS #{REC_DB} AND verified < #{VER_REC_MIN} AND name IS NOT NULL").all
			
			respond_to do |format|
				format.html # index.html.erb
				format.json { render json: @teams }
			end
		else
			@teams = Team.where("verified >= #{VERIFIED_MIN}").all
			
			@not_ver_teams = Team.where("verified < #{VERIFIED_MIN} AND name IS NOT NULL").all
			
			respond_to do |format|
				format.html # index.html.erb
				format.json { render json: @teams }
			end
		end
  end

  # GET /teams/1
  # GET /teams/1.json
  def show
    @team = Team.find(params[:id])
    
    #~ if @team.team_type == REC_DB
			#~ @verified_min = VER_REC_MIN
    #~ else
			@verified_min = VERIFIED_MIN
		#~ end

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @team }
    end
  end

  # GET /teams/new
  # GET /teams/new.json
  def new n = 1
		# Control number of teams
		n_teams = Team.where("verified >= #{VERIFIED_MIN}").count
		
		if(n_teams <= 16)
			@type ||= params[:type]
			
			if @type == PRE_STR
				@team = Team.new
				5.times {user = @team.users.build}
				@num = 5
				
				respond_to do |format|
					format.html # new.html.erb
					format.json { render json: @team }
				end
			elsif @type == REC_STR
				@team = Team.new
				
				# Number of summoners in the team
				@num = params[:num].to_i
				if @num >0
					if @num > 5
						@num = 5
					end
				else
					@num = 1
				end
				
				@num.times {user = @team.users.build}
				
				respond_to do |format|
					format.html # new.html.erb
					format.json { render json: @team }
				end
			else
				redirect_to :join
			end
		end
		redirect_to :teams, :alert => "Lo sentimos mucho, pero ya se han registrado 16 equipos."
  end

  # GET /teams/1/edit
  def edit
		#~ @team = Team.find(params[:id])
		
		unless @access_granted
			redirect_to @team, notice: "No tienes permisos para tal accion."
		end
  end

  # POST /teams
  # POST /teams.json
  def create
		# Control number of teams
		n_teams = Team.where("verified >= #{VERIFIED_MIN}").count
		
		if(n_teams <= 16)
			@team = Team.new(params[:team])
			
			if params[:team][:name]
				if params[:commit] == "Enviar"
					@team.users.each do |user|
						pass = user.randomize_password
						user.password = pass
						user.role = 2
						team = @team
						mail = UserMailer.welcome_email(user,team,pass).deliver
					end
					
					respond_to do |format|
						if @team.save			
							format.html { redirect_to @team, notice: "El equipo ha sido creado." }
							format.json { render json: @team, status: :created, location: @team }
						else
							format.html { render action: "new" }
							format.json { render json: @team.errors, status: :unprocessable_entity }
						end
					end
				else
					@type = REC_STR
					@num = 5;
					render action: "new"				
				end
			end
		end
		redirect_to :teams, :alert => "Lo sentimos mucho, pero ya se han registrado 16 equipos."
  end

  # PUT /teams/1
  # PUT /teams/1.json
  def update
    #~ @team = Team.find(params[:id])
    
		if @access_granted
			uploaded_io = params[:team][:logo]
			if uploaded_io
				File.open(Rails.root.join('public', 'logos', uploaded_io.original_filename), 'wb') do |file|
					file.write(uploaded_io.read)
				end
			end
			
			
			respond_to do |format|
				if @team.update_attributes(params[:team])
					format.html { redirect_to @team, notice: 'Team was successfully updated.' }
					format.json { head :no_content }
				else
					format.html { render action: "edit" }
					format.json { render json: @team.errors, status: :unprocessable_entity }
				end
			end
		else
			redirect_to @team, notice: "No tienes permisos para tal accion."
		end
  end

  # DELETE /teams/1
  # DELETE /teams/1.json
  def destroy
		if @access_granted
			#~ @team = Team.find(params[:id])
			@team.destroy

			respond_to do |format|
				format.html { redirect_to teams_url }
				format.json { head :no_content }
			end
    else
			redirect_to @team, notice: "No tienes permisos para tal accion."
		end
  end
  
  def check_auth
		@team = Team.find(params[:id])
		@access_granted = false
		
		if current_user
			if current_user.team.id == @team.id || current_user.role == 1
				@access_granted = true
			end
		end
	end
	
	def send_welcome
		if current_user.role == 1
		
			@team.users.each do |user|
				pass = user.randomize_password
				user.password = pass
				team = @team
				mail = UserMailer.welcome_email(user,team,pass).deliver
			end
		
			redirect_to @team, notice: "Los correos han sido enviados."
		else
			redirect_to @team, notice: "No tienes permisos para esta acción."
		end
	end
	
	def choose_type
	end
end
