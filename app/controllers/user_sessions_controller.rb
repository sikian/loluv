class UserSessionsController < ApplicationController
	#~ before_filter :require_no_user, :only => [:new, :create]
  #~ before_filter :require_user, :only => :destroy
  
  # GET /user_sessions/new
  # GET /user_sessions/new.json
  def new
		user_session = UserSession.find
		if user_session
			redirect_to current_user
		else
			@user_session = UserSession.new
			@login_wrap = true

			respond_to do |format|
				format.html # new.html.erb
				format.json { render json: @user_session }
			end
		end
  end

  # POST /user_sessions
  # POST /user_sessions.json
  def create
		user_session = UserSession.find
		if user_session
			redirect_to user_session
		else
			@user_session = UserSession.new(params[:user_session])
			@login_wrap = true
			
			respond_to do |format|
				if @user_session.save
					format.html { redirect_to current_user, notice: 'Login successful.' }
					format.json { render json: @user_session, status: :created, location: @user_session }
					
					unless current_user.verified
						current_user.verified = true
						current_user.save
						current_user.team.verified += 1
						current_user.team.save
					end
				else
					#~ format.html { render action: "new", location: login_path }
					format.html { render :new }
					format.json { render json: @user_session.errors, status: :unprocessable_entity }
				end
			end
		end
  end
  
  # DELETE /user_sessions/1
  # DELETE /user_sessions/1.json
  def destroy
    user_session = UserSession.find
    #~ user_session.destroy if @user_session
    user_session.destroy

    respond_to do |format|
      format.html { redirect_to :root, notice: 'Logged out.' }
      format.json { head :no_content }
    end
  end
end
