#!/bin/env ruby
# encoding: utf-8

class UsersController < ApplicationController
	before_filter :check_auth, :only => [:edit, :update, :destroy]

  # GET /users
  # GET /users.json
  def index		
    @users = User.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @users }
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/new
  # GET /users/new.json
  def new
    @user = User.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/1/edit
  def edit
    #~ @user = User.find(params[:id])
    unless @access_granted
			redirect_to @user, notice: "No tienes permisos para tal accion."
		end
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(params[:user])

    respond_to do |format|
      if @user.save      
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render json: @user, status: :created, location: @user }
      else
        format.html { render action: "new" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    #~ @user = User.find(params[:id])
		if @access_granted
			respond_to do |format|
				if @user.update_attributes(params[:user])
					format.html { redirect_to @user, notice: 'User was successfully updated.' }
					format.json { head :no_content }
				else
					format.html { render action: "edit" }
					format.json { render json: @user.errors, status: :unprocessable_entity }
				end
			end
		else
			redirect_to @user, notice: "No tienes permisos para tal accion."
		end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
		if @access_granted
			#~ @user = User.find(params[:id])
			@user.destroy

			respond_to do |format|
				format.html { redirect_to users_url }
				format.json { head :no_content }
			end
		else
			redirect_to @user, notice: "No tienes permisos para tal accion."
		end
  end
  
  def change_password
		@user = User.find(params[:id])
		
		valid = @user.valid_password? params[:user]["old_password"]
		
		if valid 
			@user.password = params[:user]["new_password"]
			@user.save
			@change = true
		else
			@change = false
			@false_password = true
		end
		
		render 'show'
  end
  
  def new_password
		# Ask for username and uvuser.
		#
		# If both are as expected, generate a new password and send email,
		# 	otherwise empty fields and send error.
		#
		@login_wrap = true
		
		if params[:user]
			@user = User.where(["username = ?", params[:user]["username"]]).first
			
			 if @user
				if @user.uvuser ==  params[:user]["uvuser"]
					# Correct, send email.
					
					pass = @user.randomize_password
					@user.password = pass
					mail = UserMailer.new_password_email(@user,pass).deliver
					if mail
						@user.save
						UserSession.find.destroy
						redirect_to :login, notice: "Los nuevos datos han sido enviados al email asociado a la cuenta."
					else
						UserSession.find.destroy
						@error = "Ha surgido un error al enviar el email, por favor inténtalo de nuevo o contáctanos."
					end
					
					if session = UserSession.find
						session.destroy
					end
				else
					# Incorrect: uvuser is incorrect.
					@error = "El usuario de la UV introducido no coincide con el de nuestra base de datos."
					@user = User.new
				end
			 else
			 # No user found
				@error = "No se ha encontrado ningun usuario."
				@user = User.new
			end
		else
			# No data introduced
			@user = User.new
		end
  end
  
  def update_roles
		@user = User.find(params[:id])
		
		@user.position = params[:pos]
		if @user.save
			redirect_to @user, notice: "Roles actualizados correctamente."
		else
			redirect_to @user, alert: "Ha surgido un problema actualizando los roles."
		end
  end
  
  # Seed is intended to make match-making as transparent as possible
  def update_seed
		@user = User.find(params[:id])

		if @user.update_attributes(params[:user])
			redirect_to @user, notice: "Semilla actualizada correctamente."
		else
			redirect_to @user, alert: "Ha surgido un problema actualizando la semilla."
		end
  end
  
  def check_auth
		# Just allow user 0 && admins
		@user = User.find(params[:id])
		@access_granted = false
		
		if current_user
			if current_user.id == 1 || current_user.role == 1
				@access_granted = true
			end
		end
	end
  
end
