#!/bin/env ruby
# encoding: utf-8

class UserMailer < ActionMailer::Base
  
  def welcome_email(user, team, pass)
		@user = user
		@team = team
		@pass = pass
		@url = "http://uvlol.tk"
		return mail(:to => user.uvuser.to_s + "@alumni.uv.es", :from => '"Torneo LoL UV" <etseuvlol@gmail.com>', :subject => "Bienvenido al 1er torneo UVLoL.")
	end
	
  def new_password_email(user, pass)
		@user = user
		@pass = pass
		@url = "http://uvlol.tk"
		return mail(:to => user.uvuser.to_s + "@alumni.uv.es", :from => '"Torneo LoL UV" <etseuvlol@gmail.com>', :subject => "Cuenta uvLoL - Contraseña.")
	end
	
	def mass_email(uvuser, subject, content)
		@content = content
		
		return mail(:to => uvuser + "@alumni.uv.es", 
									:from => '"Torneo LoL UV" <etseuvlol@gmail.com>',
									:subject => subject)
	end
end
