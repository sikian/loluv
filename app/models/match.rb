class Match < ActiveRecord::Base
  attr_accessible :date, :replay_id, :team1_id, :team2_id, :winner_id, :round
  
  def team1
		team = Team.find(team1_id) if team1_id
  end
  
  def team2
		team = Team.find(team2_id) if team2_id
  end
  
  def teams
		team0 = Team.new
		team0.name = "Ninguno"
		teams = [team1, team2, team0]
  end
end
