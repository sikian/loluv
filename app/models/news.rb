class News < ActiveRecord::Base
  attr_accessible :content, :published, :title, :user_id
  
  belongs_to :user
end
