class Petition < ActiveRecord::Base
  attr_accessible :answer, :answer_comments, :petition_comments, :team_id, :user_id
end
