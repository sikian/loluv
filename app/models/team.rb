#!/bin/env ruby
# encoding: utf-8

class Team < ActiveRecord::Base
  attr_accessible :name, :logo, :users_attributes, :team_type, :matching_hash
  
  has_many :users, :dependent => :destroy
  accepts_nested_attributes_for :users, :reject_if => lambda { |a| a[:username].blank? }, :allow_destroy => true
  
  validates_attachment_content_type :logo, 
		:content_type => /^image\/(jpg|jpeg|pjpeg|png|x-png)$/,
		:message => 'el formato del archivo no está permitido (sólo imágenes en jpeg/png)'
  
  has_attached_file :logo, {
		:styles => {:thumb=> ["128x128>"], :icon => ["32x32#", :png] }, 
		:default_url => "missing_logo.gif",
		:path => ":rails_root/public/logos/:id/:style/:filename",
		:url => "/logos/:id/:style/:filename"
	}
 
end
