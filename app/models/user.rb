#!/bin/env ruby
# encoding: utf-8

class User < ActiveRecord::Base
  attr_accessible :username, :uvuser, :crypted_password, :password, :persistence_token, :role, :team_id, :verified, :position, :seed
  belongs_to :team
  has_many :news
  
  acts_as_authentic do |c|
		c.validate_password_field= false
		c.require_password_confirmation = false
    #~ c.crypto_provider = Authlogic::CryptoProviders::Sha256
		c.validates_format_of_login_field_options = {
			:with => /^[[:alnum:]\s]*$/u, 
			:message => I18n.t('error_messages.login_invalid', 
				:default => "El nombre de invocador sólo debería contener letras, número, espacios y acentos."
			)
		}
  end 

  attr_accessor :password_confirmation, :old_password, :new_password
  
  def pos_a
		pos_a = Array.new
		position.each_char do |c|
			case c
			when 't'
				pos_a << "top"
			when 'j'
				pos_a << "jungler"
			when 'm'
				pos_a << "mid"
			when 'a'
				pos_a << "adc"
			when 's'
				pos_a << "support"
			end
		end
		
		return pos_a
  end  
end
