Loluv::Application.routes.draw do

  resources :petitions


	root :to => 'pages#index'

	# Pre (higher priority)
	match 'users' => 'teams#index'
	
	# Resources (default)
	resources :users
	#~ resources :replays
  resources :teams
  
  # Extend matches
	resources :matches	
	match 'round' => 'matches#index'
	match 'round/:round' => 'matches#index', :as => :matches_round
  
  # Extend teams
  match 'teams/new/:type' => 'teams#new', :as => :teams_new_type
  match 'teams/index/:type' => 'teams#index', :as => :teams_index_type
  match 'teams/new/:type/:num' => 'teams#new', :as => :recruit_num
  #~ match 'join/:type/' => 'teams#new', :as => :join_type
  #~ match 'join/:type/:num' => 'teams#new', :as => :recruit_num
	
	# Admin
	resources :news
	match 'admin/send_email' => 'pages#admin_send_email'
	match 'admin/make_matches' => 'pages#make_matches'
	
	resources :teams do
		member do
			post 'send_welcome'
		end
	end
	
	# Pages
	match 'index' => 'pages#index'
	match 'info' => 'pages#info', :as => :info
	match 'info/bases' => 'pages#bases', :as => :bases
	match 'info/matchmaking' => 'pages#match_making_info', :as => :matchmakinginfo
	#~ match 'join' => 'teams#choose_type', :as => :join
	
	# User sessions
	resources :user_sessions
	match 'login' => 'user_sessions#new', :as => :login
	match 'logout' => 'user_sessions#destroy', :as => :logout
	
	# User stuff (change password, change roles...)
	match 'users/change_password/:id/' => 'users#change_password', :as => :change_password
	match 'users/:id/update_roles/:pos' => 'users#update_roles', :as => :update_roles
	match 'users/:id/update_seed' => 'users#update_seed', :as => :update_seed
	
	# Request new password
	match 'new_password' => 'users#new_password', :as => :req_new_password
	match 'send_new_password' => 'users#new_password', :as => :send_new_password
	
	resources :user_sessions

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
