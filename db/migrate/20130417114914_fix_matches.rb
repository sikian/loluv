class FixMatches < ActiveRecord::Migration
	def change
		add_column :teams, :team1_id, :integer
		add_column :teams, :team2_id, :integer
		remove_column :teams, :team_id
	end
end
