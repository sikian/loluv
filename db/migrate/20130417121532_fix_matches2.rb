class FixMatches2 < ActiveRecord::Migration
	def change
		remove_column :teams, :winner
		add_column :teams, :winner, :integer	
	end
end
