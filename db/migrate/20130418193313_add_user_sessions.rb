class AddUserSessions < ActiveRecord::Migration
  def up
    create_table :user_sessions do |t|
		  t.datetime "created_at", :null => false
		  t.datetime "updated_at", :null => false
    end
  end
 
  def down
    drop_table :user_sessions
  end
end
