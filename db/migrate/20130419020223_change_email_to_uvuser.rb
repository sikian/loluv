class ChangeEmailToUvuser < ActiveRecord::Migration
  def up
  	remove_column :users, :email
  	add_column :users, :uvuser, :string
  end

  def down
  	remove_column :users, :uvuser
  	add_column :users, :email, :string
  end
end
