class ChangeNameToUserInUsers < ActiveRecord::Migration
  def up
  	remove_column :users, :name
  	add_column :users, :user, :string
  end

  def down
  	remove_column :users, :user
  	add_column :users, :name, :string
  end
end
