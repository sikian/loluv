class ChangeNameToUsernameInUsers < ActiveRecord::Migration
  def up
  	remove_column :users, :user
  	add_column :users, :username, :string
  end

  def down
  	remove_column :users, :username
  	add_column :users, :user, :string
  end
end
