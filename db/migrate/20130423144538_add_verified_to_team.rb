class AddVerifiedToTeam < ActiveRecord::Migration
  def change
  	add_column :teams, :verified, :integer, :default => 0
  end
end
