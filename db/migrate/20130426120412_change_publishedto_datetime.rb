class ChangePublishedtoDatetime < ActiveRecord::Migration
  def change
  	change_column :news, :published, :datetime
  end
end
