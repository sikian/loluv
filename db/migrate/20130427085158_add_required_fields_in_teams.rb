class AddRequiredFieldsInTeams < ActiveRecord::Migration
  def change
		change_column :teams, :name, :string, :null => false
		change_column :users, :username, :string, :null => false
		change_column :users, :uvuser, :string, :null => false
		change_column :users, :role, :integer, :default => 2
  end
end
