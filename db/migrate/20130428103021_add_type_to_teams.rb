class AddTypeToTeams < ActiveRecord::Migration
  def change
  	add_column :teams, :type, :integer, :null => false, :default => 0
  end
end
