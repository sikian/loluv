class RenameTypeToTeamstype < ActiveRecord::Migration
  def change
  	rename_column :teams, :type, :team_type
  end
end
