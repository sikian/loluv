class CreatePetitions < ActiveRecord::Migration
  def change
    create_table :petitions do |t|
      t.integer :user_id
      t.integer :team_id
      t.text :petition_comments
      t.boolean :answer
      t.text :answer_comments
      
      t.timestamps
    end
  end
end
