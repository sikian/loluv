class ChangeHashToMatchingHash < ActiveRecord::Migration
  def change
  	rename_column :teams, :hash, :matching_hash
  end
end
