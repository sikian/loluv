class ChangeWinnerToInteger < ActiveRecord::Migration
  def change
  	change_column :matches, :winner_id, :integer
  end
end
